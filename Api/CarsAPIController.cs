﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using CarsAPIWeb.Managers;
using Newtonsoft.Json;

namespace CarsAPIWeb.Api
{
    public class CarsApiController : ApiController
    {
        private ICarService CarService { get; set; }

        public CarsApiController():this(new CarService())
        {

        }
        public CarsApiController(ICarService carService)
        {
            CarService = carService;
        }
        // GET api/carsapi/getavailablecars
        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("getavailablecars")]
        public IHttpActionResult GetAvailableCars()
        {
            try
            {
                var carData = CarService.GetAvailableCars();
                if (carData != null)
                {
                    return Ok(JsonConvert.SerializeObject(carData));
                }
                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
          
        }
        // POST api/carsapi/bookcar/1
        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("bookcar")]
        [ValidateAntiForgeryToken]
        public IHttpActionResult BookCar([FromUri]int id)
        {
            try
            {
                CarService.BookCar(id);
                return Ok();
            }
            catch (Exception)
            {
               return InternalServerError();
            }
           
        }

       
    }
}