﻿using System.Web.Http;
using System.Web.Routing;

namespace CarsAPIWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            DependencyInjectionConfig.RegisterDependencies();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
