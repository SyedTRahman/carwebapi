﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Xml;

namespace CarsAPIWeb.Managers
{
    public class CarService : ICarService
    {
        public IEnumerable<XmlNode> GetAvailableCars()
        {
            double noOfHours;
            double.TryParse(ConfigurationManager.AppSettings["NoOfHoursToBook"], out noOfHours);
            var data = GetCarData();
            var xmlNodeList = data?.SelectNodes("/Cars/Car");
            var availbeCars = xmlNodeList?.Cast<XmlNode>().Where(node =>
            {
                var xmlElementAvailable = node["Available"];
                var xmlElementTime = node["BookedDate"];
                TimeSpan span = new TimeSpan();
                if (!string.IsNullOrEmpty(xmlElementTime?.InnerText))
                {
                    var dateValue = DateTime.ParseExact(xmlElementTime?.InnerText,
                        "MM/dd/yyyy HH:mm:ss",
                        CultureInfo.InvariantCulture);
                    span = DateTime.UtcNow.Subtract(dateValue);
                }
                return (xmlElementAvailable != null && xmlElementAvailable.InnerText == "1") || xmlElementTime!=null && span.TotalHours > noOfHours;
            });
            return availbeCars;
        }

        private XmlDocument GetCarData()
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/CarData.xml"));
            return xml;
        }

        public void BookCar(int carId)
        {
            var data = GetCarData();
            var xmlNode = data?.SelectSingleNode("/Cars/Car[CarId='"+carId+"']");
            if (xmlNode != null)
            {
                var xmlElement = xmlNode["Available"];
                if (xmlElement != null) xmlElement.InnerText = "0";
                var xmlElementDate = xmlNode["BookedDate"];
                if (xmlElementDate != null) xmlElementDate.InnerText = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture);
                data.Save(HttpContext.Current.Server.MapPath("~/App_Data/CarData.xml"));
            }
        }
    }
}