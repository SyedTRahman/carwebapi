﻿using System.Collections.Generic;
using System.Xml;

namespace CarsAPIWeb.Managers
{
    public interface ICarService
    {
        IEnumerable<XmlNode> GetAvailableCars();
        void BookCar(int carId);
    }
}