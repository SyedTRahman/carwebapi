﻿using System;
using CarsAPIWeb.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace CarsAPIWeb
{
    public class DependencyInjectionConfig
    {
        public static IServiceProvider RegisterDependencies()
        {
            var serviceProvider = new ServiceCollection()
                .AddTransient<ICarService, CarService>()
                .BuildServiceProvider();
            return serviceProvider;
        }
    }
}