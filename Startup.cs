﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CarsAPIWeb.Startup))]
namespace CarsAPIWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
